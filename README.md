# README #

### What is this repository for? ###

* This repo will be used for building automated images for use in CI etc.

### How do I get set up? ###

* Make sure you have docker on your local machine to build the images and go to the desired directory

### Contribution guidelines ###

* Create issues if you encounter a problem
* I Accept PR's if you fix an issue